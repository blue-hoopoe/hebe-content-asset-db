const csvW = require('csv-writer');
const path = require('path');
const settings = require('./config');

const searchSlots = require('./scripts/searchSlots');

const timestamp = Math.floor(new Date() / 1000);

(async () => {

    console.log('Searching Slots...');
    const matches = await searchSlots(
        path.resolve(settings.input)
    );

    console.log('Writing CSV...');
    let csv = csvW.createArrayCsvWriter({
        header: ['CONTENT_ID'],
        path: path.resolve(settings.output, `slots-${timestamp}.csv`)
    });
    await csv.writeRecords(matches.map(match => [match]));
})();

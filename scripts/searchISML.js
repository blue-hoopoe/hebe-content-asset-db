const findInFiles = require('find-in-files');

const pattern = /aid="\S*"/;

module.exports = async (path) => {
    const scan = await findInFiles.find(pattern, path, `.isml$`);
    const results = [];
    Object.values(scan).forEach(result => {
        result.matches.forEach(match => {
            results.push(
                match.split(`"`)[1]
            );
        });
    });
    return results;
}
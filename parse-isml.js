const csvW = require('csv-writer');
const path = require('path');
const settings = require('./config');

const searchISML = require('./scripts/searchISML');

const timestamp = Math.floor(new Date() / 1000);

(async () => {

    console.log('Searching ISML...');
    let matches = await searchISML(
        path.resolve(settings.cartridges)
    );

    console.log('Writing CSV...');
    let csv = csvW.createArrayCsvWriter({
        header: ['CONTENT_ID'],
        path: path.resolve(settings.output, `isml-${timestamp}.csv`)
    });
    await csv.writeRecords(matches.map(match => [match]));

})();
